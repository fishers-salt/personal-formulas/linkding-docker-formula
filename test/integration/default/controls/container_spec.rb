# frozen_string_literal: true

describe docker_container(name: 'linkding') do
  it { should exist }
  it { should be_running }
  its('image') { should eq 'sissbruecker/linkding:latest' }
  it { should have_volume('/etc/linkding/data', '/srv/docker/linkding/data') }
end
