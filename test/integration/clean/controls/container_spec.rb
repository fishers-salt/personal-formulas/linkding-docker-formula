# frozen_string_literal: true

describe docker.containers do
  its('names') { should_not include 'linkding' }
  its('images') { should_not include 'sissbruecker/linkding:latest' }
end
