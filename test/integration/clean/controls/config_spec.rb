# frozen_string_literal: true

directories = %w[
  docker/linkding/data
]

directories.each do |dir|
  control "linkding-docker-config-file-#{dir}-managed" do
    title 'should still exist'

    describe directory("/srv/#{dir}") do
      it { should exist }
      it { should be_owned_by 'kitchen' }
      it { should be_grouped_into 'users' }
      its('mode') { should cmp '0755' }
    end
  end
end
