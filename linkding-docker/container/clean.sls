# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as linkding_docker with context %}

include:
  - {{ sls_config_clean }}

{%- set name = linkding_docker.container.name %}
{%- set image = linkding_docker.container.image %}

linkding-docker-container-clean-container-absent:
  docker_container.absent:
    - name: {{ name }}
    - force: True

linkding-docker-docker-clean-image-absent:
  docker_image.absent:
    - name: {{ image }}
    - require:
      - linkding-docker-container-clean-container-absent
