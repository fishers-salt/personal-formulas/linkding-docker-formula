# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as linkding_docker with context %}

{%- set mountpoint = linkding_docker.docker.mountpoint %}

linkding-docker-mountdir-managed:
  file.directory:
    - name: {{ mountpoint }}/docker/linkding
    - makedirs: True

linkding-docker-image-present:
  docker_image.present:
    - name: {{ linkding_docker.container.image }}
    - tag: {{ linkding_docker.container.tag }}
    - force: True

linkding-docker-container-running:
  docker_container.running:
    - name: {{ linkding_docker.container.name }}
    - image: {{ linkding_docker.container.image }}:{{ linkding_docker.container.tag }}
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/linkding/data:/etc/linkding/data
    - port_bindings:
      - 9090:9090
    - dns: {{ linkding_docker.dns_servers }}
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.linkding-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.linkding-web.middlewares=linkding-redirect-websecure"
      - "traefik.http.routers.linkding-web.rule=Host(`{{ linkding_docker.container.url }}`)"
      - "traefik.http.routers.linkding-web.entrypoints=web"
      - "traefik.http.routers.linkding-websecure.rule=Host(`{{ linkding_docker.container.url }}`)"
      - "traefik.http.routers.linkding-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.linkding-websecure.tls=true"
      - "traefik.http.routers.linkding-websecure.entrypoints=websecure"
