# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_container_running = tplroot ~ '.container.running' %}
{%- from tplroot ~ "/map.jinja" import mapdata as linkding_docker with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_container_running }}

{%- set mountpoint = linkding_docker.docker.mountpoint %}
{%- set user = linkding_docker.owner.user %}
{%- set group = linkding_docker.owner.group %}

{%- for dir in [
  'docker/linkding/data',
  ]
%}
linkding-docker-config-file-{{ dir|replace('/', '_') }}-directory-managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: '0755'
    - file_mode: '0644'
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}
